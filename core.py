import cv2

import os
import numpy as np


def bounding_boxes_coordinates(file_path):
    file1 = open(file_path, 'r')
    Lines = file1.readlines()

    count = 0
    frames = []
    # Strips the newline character
    for line in Lines:
        count += 1
        elements = line.strip().split(" ")
        frames.append(elements)
    return frames


def crops_generation(frames, images_path, crops_path, c, key):
    if not os.path.exists(crops_path):
        os.makedirs(crops_path)
    if not os.path.exists(os.path.join(crops_path, f"{key}")):
        os.makedirs(os.path.join(crops_path, f"{key}"))
    for fr in frames:
        frame, exist = int(fr[0]), int(fr[1])
        if exist == 1:
            x, y, h, w = int(fr[2]), int(fr[3]), int(fr[4]), int(fr[5])
            img = cv2.imread(images_path + "/frame%d.jpg" % (frame - 1))
            crop_img = img[y:y + h, x:x + w]
            cv2.imwrite(os.path.join(crops_path, f"{key}") + "/crop%d.jpg" % c, crop_img)
            c += 1
    return c


def frames_extraction(video_path, frames_path):
    import cv2
    if not os.path.exists(frames_path):
        os.makedirs(frames_path)
        cap = cv2.VideoCapture(video_path)
        length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        success, image = cap.read()
        count = 0
        while success:
            cv2.imwrite(frames_path + "/frame%d.jpg" % count, image)  # save frame as JPEG file
            success, image = cap.read()
            print('Read a new frame: ', success)
            count += 1
    else:
        print("file already exist")


def saliency_maps_coordinates(dir_path, saliency_coordinates_file_path):
    if not os.path.exists(saliency_coordinates_file_path):
        open(saliency_coordinates_file_path, 'a').close()

    for i, path in enumerate(sorted(os.listdir(dir_path))):

        print(dir_path + "/" + path)
        img = cv2.imread(dir_path + "/" + path)

        # convert the image to grayscale
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # apply thresholding on the gray image to create a binary image
        ret, thresh = cv2.threshold(gray, 127, 255, 0)

        # find the contours
        contours, _ = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        # take the first contour
        if len(contours) != 0:
            cnt = contours[0]

            # compute the bounding rectangle of the contour
            x, y, w, h = cv2.boundingRect(cnt)
            listframes = [str(i), "1", str(x), str(y), str(w + 50), str(h + 50)]
            print(listframes)


        elif len(contours) == 0:
            listframes = [str(i), "0"]
            print(listframes)

        with open(fr"{saliency_coordinates_file_path}", "a", encoding="utf-8") as fo:
            fo.write(' '.join([''.join(i) for i in listframes]))
            fo.write('\n')
