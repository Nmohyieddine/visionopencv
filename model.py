from datetime import datetime

import tensorflow as tf
print(f'TensorFlow version: {tf.__version__}')
print(f'Keras version: {tf.keras.__version__}')
from keras import layers
import keras as keras
import matplotlib.pyplot as plt
from keras.preprocessing.image import ImageDataGenerator

TRAIN = r"/home/mohyi/Desktop/GITW_light2_TD/data_train"
TEST1 = r"/home/mohyi/Desktop/GITW_light2_TD/data_test1"
TEST2 = r"/home/mohyi/Desktop/GITW_light2_TD/data_test2"
MODEL_PATH = r"models/my_model1"



def generators(shape,preprocessing,train_path,test_path,batch_size):
    '''Create the training and validation datasets for
    a given image shape.
    '''
    imgdatagen = ImageDataGenerator(
        preprocessing_function = preprocessing,
        horizontal_flip = True,
        validation_split = 0.1,
    )

    height, width = shape

    train_dataset = imgdatagen.flow_from_directory(
        directory=train_path,
        target_size = (height, width),
        class_mode = "categorical",
        batch_size = batch_size,
        subset = 'training',
    )

    val_dataset = imgdatagen.flow_from_directory(
        directory=train_path,
        target_size = (height, width),
        class_mode = "categorical",
        batch_size = batch_size,
        subset = 'validation'
    )

    test_dataset = imgdatagen.flow_from_directory(
        directory=test_path,
        target_size=(height, width),
        class_mode="categorical",
        batch_size=batch_size,
        subset='validation'
    )

    return train_dataset, val_dataset,test_dataset, train_dataset.class_indexes


def plot_history(history, yrange):
    '''Plot loss and accuracy as a function of the epoch,
    for the training and validation datasets.
    '''
    acc = history.history['acc']
    val_acc = history.history['val_acc']
    loss = history.history['loss']
    val_loss = history.history['val_loss']

    # Get number of epochs
    epochs = range(len(acc))

    # Plot training and validation accuracy per epoch
    plt.plot(epochs, acc)
    plt.plot(epochs, val_acc)
    plt.title('Training and validation accuracy')
    plt.ylim(yrange)
    plt.savefig('plot/accuracy.png')
    # Plot training and validation loss per epoch
    plt.figure()

    plt.plot(epochs, loss)
    plt.plot(epochs, val_loss)
    plt.title('Training and validation loss')
    plt.savefig('plot/loss.png')

    plt.show()

def classifier(input_model):

    x = layers.Flatten()(input_model.output)
    # three hidden layers
    x = layers.Dense(100, activation='relu')(x)
    x = layers.Dense(100, activation='relu')(x)
    x = layers.Dense(100, activation='relu')(x)
    predictions = keras.layers.Dense(9, activation='softmax')(x)

    return predictions


def train_model(train_dataset,val_dataset, label_map,lr=0.01,epochs=5,workers=10):

    logdir = "logs/scalars/" + datetime.now().strftime("%Y%m%d-%H%M%S")
    tensorboard_callback = keras.callbacks.TensorBoard(log_dir=logdir)

    # Model
    vgg16 = keras.applications.vgg16
    conv_model = vgg16.VGG16(weights='imagenet', include_top=False,input_shape=(224,224,3))
    conv_model.summary()

    # Classification part
    predictions = classifier(conv_model)

    # creating the full model:
    full_model = keras.models.Model(inputs=conv_model.input, outputs=predictions)
    full_model.summary()

    # freezing layers
    for layer in conv_model.layers:
        layer.trainable = False

    full_model.compile(loss='binary_crossentropy',
                      optimizer=keras.optimizers.Adamax(lr=lr),
                      metrics=['acc'])

    history = full_model.fit_generator(
        train_dataset,
        validation_data=val_dataset,
        workers=workers,
        epochs=epochs,
        callbacks=[tensorboard_callback]
    )

    # Saving model
    full_model.save('models/my_model1_sm')

    # plot accuracy and loss
    plot_history(history, yrange=(0.9,1))



def test(model_path,test_dataset):
    model = keras.models.load_model(model_path)
    score = model.evaluate(test_dataset, steps=24)
    #plot_history(loss,yrange=(0.9,1))
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])

def fine_tuning(model_path, data_finetuning):

    model = keras.models.load_model(model_path)

    predictions = classifier(model)

    full_model = keras.models.Model(inputs=conv_model.input, outputs=predictions)











