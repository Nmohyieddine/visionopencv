import sys
import getopt

from model import *
from main import *

TRAIN = r"/home/mohyi/Desktop/GITW_light2_TD/train1"
TEST1 = r"/home/mohyi/Desktop/GITW_light2_TD/test1"
TEST2 = r"/home/mohyi/Desktop/GITW_light2_TD/test2"

TRAIN_PREPARE_SM = r"/home/mohyi/Desktop/GITW_light2_TD/data_train_sm"
TEST1_PREPARE_SM = r"/home/mohyi/Desktop/GITW_light2_TD/data_test1_sm"
TEST2_PREPARE_SM = r"/home/mohyi/Desktop/GITW_light2_TD/data_test2_sm"

TRAIN_PREPARE_BB = r"/home/mohyi/Desktop/GITW_light2_TD/data_train_bb"
TEST1_PREPARE_BB = r"/home/mohyi/Desktop/GITW_light2_TD/data_test1_bb"
TEST2_PREPARE_BB = r"/home/mohyi/Desktop/GITW_light2_TD/data_test2_bb"

MODEL_PATH = r"models/my_model1"


def myfunc(argv):
    arg_data = ""
    arg_prepare = ""
    arg_mode = ""
    arg_mode_action = ""
    arg_train_epochs = ""
    arg_batch_size = ""
    arg_lr = ""
    arg_help = "{0} -p <prepare> -d <data> -m <mode> -e <train_epochs> -bs <arg_batch_size> -lr <lr>".format(argv[0])

    try:
        opts, args = getopt.getopt(argv[1:], "h:p:d:m:a:e:b:lr")
    except:
        print(arg_help)
        sys.exit(2)

    for opt, arg in opts:
        if opt in ['-h']:
            print(arg_help)  # print the help message
            sys.exit(2)
        elif opt in ['-p']:
            arg_prepare = arg
        elif opt in ['-d']:
            arg_data = arg
        elif opt in ['-m']:
            arg_mode = arg
        elif opt in ['-a']:
            arg_mode_action = arg
        elif opt in ['-e']:
            arg_train_epochs = arg
        elif opt in ['-b']:
            arg_batch_size = arg
        elif opt in ['-l']:
            arg_lr = arg

    print('arg_prepare:', arg_prepare)
    print('data:', arg_data)
    print('mode:', arg_mode)
    print('mode:', arg_mode_action)
    print('train_epochs:', arg_train_epochs)
    print('arg_batch_size:', arg_batch_size)
    print('lr:', arg_lr)

    return arg_prepare, arg_data, arg_mode, arg_mode_action, arg_train_epochs, arg_batch_size, arg_lr


def generators(shape, preprocessing, train1_path, test1_path, test2_path, batch_size):
    imgdatagen = ImageDataGenerator(
        preprocessing_function=preprocessing,
        horizontal_flip=False,
        validation_split=0.1,
    )

    height, width = shape

    train_dataset = imgdatagen.flow_from_directory(
        directory=train1_path,
        target_size=(height, width),
        class_mode="categorical",
        batch_size=batch_size,
        subset='training',
    )

    val_dataset = imgdatagen.flow_from_directory(
        directory=train1_path,
        target_size=(height, width),
        class_mode="categorical",
        batch_size=batch_size,
        subset='validation'
    )

    test1_dataset = imgdatagen.flow_from_directory(
        directory=test1_path,
        target_size=(height, width),
        class_mode="categorical",
        batch_size=batch_size,
    )

    test2_dataset = imgdatagen.flow_from_directory(
        directory=test2_path,
        target_size=(height, width),
        class_mode="categorical",
        batch_size=batch_size,
    )

    return train_dataset, val_dataset, test1_dataset, test2_dataset, train_dataset.class_indices


if __name__ == "__main__":
    arg_prepare, arg_data, arg_mode, arg_mode_action, arg_epochs, arg_batch_size, arg_lr = myfunc(sys.argv)
    if arg_prepare != "":
        print("PREPARING DATASET")
        if arg_data == "train1":
            generate_dataset(TRAIN, arg_mode, "train")
        elif arg_data == "test1":
            print("test1")
            generate_dataset(TEST1, arg_mode, "test1")
        elif arg_data == "test2":
            print("test2")
            generate_dataset(TEST2, arg_mode, "test2")
    else:
        vgg16 = keras.applications.vgg16
        if arg_mode == "bb":
            train1_path = TRAIN_PREPARE_BB
            test1_path = TEST1_PREPARE_BB
            test2_path = TEST2_PREPARE_BB
        elif arg_mode == "sm":
            train1_path = TRAIN_PREPARE_SM
            test1_path = TEST1_PREPARE_SM
            test2_path = TEST2_PREPARE_SM

        train_dataset, val_dataset, test1_dataset, test2_dataset, class_indexes = generators((224, 224),
                                                                                             preprocessing=vgg16.preprocess_input,
                                                                                             train1_path=train1_path,
                                                                                             test1_path=test1_path,
                                                                                             test2_path=test2_path,
                                                                                             batch_size=30)

        print("SKIP PREPARING DATASET")
        if arg_mode_action == "train":
            train_model(train_dataset, val_dataset, class_indexes, arg_lr, arg_epochs)

        elif arg_mode_action == "test":

            test(MODEL_PATH, test1_dataset)

        elif arg_mode_action == "fine_tuning":

            fine_tuning(MODEL_PATH, test2_dataset)
