from core import *
import os
_DATASETPATH = "/home/mohyi/Desktop/GITW_light2_TD"
_TRAINPATH = '/train1'
_TEST1PATH = '/test1'
_TEST2PATH = '/test2'

paths = {'Bowl': [], 'CanOfCocaCola': [], 'Jam': [],
         'MilkBottle': [], 'Mug': [], 'OilBottle': [], 'Rice': [],
         'Sugar': [], 'VinegarBottle': []}

_BBFILEDATAPATH="SilencyMapsframes.txt"
_IMAGEPATH="frames_subject1"
_CROPPATH="cropsfrmames_saliency1"
_VIDEOPATH="BowIplace1Subject4.mp4"
_STOREFRAMEPATH="frames_subject1"
_SALIANCYIMAGESPATH="SaliencyMaps"
_SALIANCYFILEPATH="cropsframes_saliency1"

# TODO: explore the working tree and generate the train and test dataset
def generate_dataset(data_path,mode,denomination):
    for key, value in paths.items():
        path = data_path+f"/{key}"
        list_dir = os.listdir(path)
        paths[key] = list_dir
        c = 0
        for l in list_dir:
            list_dir1 = os.listdir(path + f"/{l}")
            exist_bboxe = False
            for ld in list_dir1:
                if ld.find(".mp4") != -1:
                    video_path = path + f"/{l}" +f"/{ld}"
                elif ld.find("_bboxes.txt") != -1:
                    bboxes_path = path + f"/{l}" + f"/{ld}"
                    exist_bboxe = True
                # TODO: add condition if the bboxes don't exist
                elif ld.find("SaliencyMaps") != -1:
                    saliency_path = path+ f"/{l}" + f"/{ld}"
                elif ld.find("fixation_points.txt") != -1:
                    fixation_points_path = path + f"/{l}" + f"/{ld}"

            frames_extraction(video_path, path + f"/{l}" + "/frames")
            if mode == "bb":
                if exist_bboxe == True:
                    data = bounding_boxes_coordinates(bboxes_path)
                    c = crops_generation(data, path + f"/{l}" + "/frames", _DATASETPATH + f"/data_{denomination}_bb", c, key)
            elif mode == "sm":
                saliency_maps_coordinates(saliency_path, saliency_path+".txt")
                data = bounding_boxes_coordinates(saliency_path+".txt")
                c = crops_generation(data, path + f"/{l}" + "/frames", _DATASETPATH + f"/data_{denomination}_sm", c, key)


            # TODO: launch the process for all the objects


#extractSiliencyBBCoordinate(_SALIANCYIMAGESPATH,_SALIANCYFILEPATH)

