from tensorflow import keras
from keras.utils import load_img, img_to_array
import numpy as np

labels = {'Bowl': 0, 'CanOfCocaCola': 1, 'Jam': 2, 'MilkBottle': 3, 'Mug': 4, 'OilBottle': 5, 'Rice': 6, 'Sugar': 7}
vgg16 = keras.applications.vgg16
model = keras.models.load_model('models/my_model')
img_path = 'test_image3.png'

# loading the image:
img = load_img(img_path, target_size=(224, 224))
# turn it into a numpy array
x = img_to_array(img)
print(np.min(x), np.max(x))
print(x.shape)
# expand the shape of the array,
# a new axis is added at the beginning:
xs = np.expand_dims(x, axis=0)
print(xs.shape)
# preprocess input array for VGG16
xs = vgg16.preprocess_input(xs)
# evaluate the model to extract the features
features = model.predict(xs)
y_classes = features.argmax(axis=-1)

key_list = list(labels.keys())
val_list = list(labels.values())

# print key with val 100
position = val_list.index(y_classes)

print(features)
print(key_list[position])

